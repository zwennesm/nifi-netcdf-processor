# NetCDF processor

A simple Apache NiFi processor that converts NetCDF files to CSV. Still work in progress and definitely not 
ready for production. Preferably I would like to use the `netcdf-java` package to handle the conversion. Only
has been tested on KNMI data (https://www.knmi.nl/kennis-en-datacentrum/achtergrond/data-ophalen-vanuit-een-script)