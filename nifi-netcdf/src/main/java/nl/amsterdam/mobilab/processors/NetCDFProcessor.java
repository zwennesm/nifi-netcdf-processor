/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.amsterdam.mobilab.processors;

import org.apache.commons.io.IOUtils;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.io.InputStreamCallback;
import org.apache.nifi.processor.io.OutputStreamCallback;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.nifi.util.StringUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Tags({"NetCDF", "CSV"})
@CapabilityDescription("Modify NetCDF files to CSV rows")
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute = "", description = "")})
@WritesAttributes({@WritesAttribute(attribute = "", description = "")})
public class NetCDFProcessor extends AbstractProcessor {

    public static final PropertyDescriptor LINES_TO_SKIP = new PropertyDescriptor
            .Builder()
            .name("LINES_TO_SKIP")
            .displayName("Header lines to skip")
            .description("NetCDF files often contain a description of the fields. These need to be skipped.")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    public static final PropertyDescriptor HEADER = new PropertyDescriptor
            .Builder()
            .name("HEADER")
            .displayName("Comma separated header")
            .description("Comma separated header list of column names (no spaces)")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    public static final Relationship SUCCESS = new Relationship.Builder()
            .name("success")
            .description("Parsed CSV rows")
            .build();

    public static final Relationship FAILURE = new Relationship.Builder()
            .name("failure")
            .description("Unable to parse NetCDF file")
            .build();

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(LINES_TO_SKIP);
        descriptors.add(HEADER);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(SUCCESS);
        relationships.add(FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        int skipLines = Integer.parseInt(context.getProperty(LINES_TO_SKIP).getValue());
        String header = context.getProperty(HEADER).getValue();

        final AtomicReference<String> result = new AtomicReference<>();
        FlowFile flowFile = session.get();

        if (flowFile == null) {
            return;
        }

        session.read(flowFile, in -> {
            try {
                String netCDF = IOUtils.toString(in, Charset.defaultCharset());
                result.set(toCSV(netCDF, skipLines, header));
            } catch (Exception ex) {
                ex.printStackTrace();
                getLogger().error("error: " + ex.getMessage());
            }
        });

        flowFile = session.write(flowFile, out -> out.write(result.get().getBytes()));
        session.transfer(flowFile, SUCCESS);
    }

    /**
     * @param netCDF    InputStream containing the netCDF data
     * @param skipLines amount of lines to skip in the top of the file
     * @return a new InputStream containing the cleaned up lines in CSV format
     */
    public String toCSV(String netCDF, int skipLines, String header) {
        String newlineCharacter = "\n";

        List<String> lines = Arrays.stream(netCDF.split(newlineCharacter))
                .skip(skipLines)
                .map(line -> line.replace(" ", ""))
                .collect(Collectors.toList());

        return header + newlineCharacter + StringUtils.join(lines, newlineCharacter);
    }
}
