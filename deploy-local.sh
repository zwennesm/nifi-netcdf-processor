export JAVA_HOME=$(/usr/libexec/java_home -v 1.8.0_262)

echo "building new version for app"
mvn clean install

mv nifi-netcdf-nar/target/nifi-netcdf-processor-1.0.nar .

echo "copying new version to docker container"
docker cp \
  nifi-netcdf-processor-1.0.nar \
  nifi:/opt/nifi/nifi-current/lib/nifi-netcdf-processor-1.0.nar

echo "restarting Apache Nifi"
docker restart nifi